# Herramientas de ciberseguridad

## Introducción
En este proyecto, se va a explicar algunos de los ataques más comunes que se producen en el ámbito de la ciberseguridad, además, se va a explicar cuales son las herramientas con las que se pueden realizar estos ataques.

El  objetivo principal de este proyecto es concienciar de los riesgos que supone no tener en cuenta lo insegura que es la red, y tener precaución por si en algún momento se la máquina que se está utilizando comienza a hacer cosas poco comunes de un día para otro, valorar que lo más posible es que haya sufrido un ataque.

## Sistema operativo
### [Kali Linux](kali_linux/README.md)

## Ataques
### MITM
- [Introducción](MITM/1_introduccionMITM.md)
- [Interceptación](MITM/2_interceptacion.md)
- [Práctica intercepcación](MITM/2.1_practicaInterceptacion.md)
- [Desencriptación](MITM/3_desencriptacion.md)
- [Práctica desencriptación](MITM/3.1_practicaDesencriptacion.md)
### DoS
- [Introducción](Android/introduccionAndroid.md)
- [Práctica](Android/practicaAndroid.md)
### Android
- [Introducción](Android/introduccionDoS.md)
- [Práctica](Android/practicaDoS.md)


