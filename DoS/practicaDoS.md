# Práctica ataque DoS
Para esta práctica, se va a seguir usando las dos máquinas virtuales anteriores, Kali Linux (atacante) y Centos8 (víctima). En la máquina Centos, está abierto el puerto 80. Esto se puede mirar con:
```bash
ufw status
```
En la máquina Kali, primero se puede comprobar que las dos máquinas se ven, haciendo ping. Una vez comprobado esto, se puede utilizar el programa hping3, el cual será el que lance este ataque. Este programa tiene diferentes parámetros:
- hping3: nombre del programa de línea de comandos que se utiliza para enviar paquetes de red.
- c: envía x número de paquetes al servidor de destino.
- d: especifica el tamaño del paquete a enviar.
- S: establece la bandera TCP SYN en los paquetes enviados.
- w: especifica el tamaño de la ventana TCP.
- p: especifica el puerto de destino.
- flood: envía paquetes lo más rápido posible, sin esperar la respuesta del servidor de destino.
- IP de la máquina: dirección IP del servidor de destino.

Para probar este ataque, se utilizará el siguiente comando: 
```bash
hping3 -c 10000 -d 120 -S -w 64 -p 80 --flood 192.168.3.6
```
por lo que, sabiendo los parámetros citados anteriormente, se puede deducir su funcionamiento final: 
Este comando enviará 10000 paquetes TCP SYN a la dirección IP de destino en el puerto 80. El parámetro --flood permite enviar los paquetes a alta velocidad, lo que puede sobrecargar el servidor y causar una denegación de servicio.

![Ataque DoS](Imagenes/Imagen17.png)

Y si todo ha salido bien, deberá salir en la barra de arriba de Kali Linux (la cual muestra el uso de CPU), una incrementación significativa del uso de CPU. 
En la máquina víctima, dentro del navegador, si se busca cualquier página, esta no cargará debido a la sobrecarga que está sufriendo mi máquina.