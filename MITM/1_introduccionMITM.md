# MITM
## En qué consiste
Este método de ataque consiste en obtener datos confidenciales (números de tarjeta, credenciales de inicio de sesión, detalles de cuentas bancarias…). Esto es todo un riesgo ya que, si se consigue llevarse a cabo, se puede sufrir suplantación de identidad. Este ataque consta de dos fases, la primera conocida como __interceptación__ y la segunda __desencriptación__.