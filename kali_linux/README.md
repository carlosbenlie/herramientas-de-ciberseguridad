# Kali Linux
Kali Linux es un sistema operativo basado en Debian GNU/Linux y fue desarrollado por la compañía de ciberseguridad Offensive Security.

La ventaja que tiene este sistema operativo es que tiene más de 600 programas destinados al hacking ético, y todos ellos instalados por defecto en la máquina, por lo que hace más fácil la labor de ciberseguridad.

Kali Linux tiene diversas funciones, las principales son:
- Recopilación de información: obtiene datos de un objetivo cualquiera.
- Análisis de vulnerabilidades: escaneo e identificación de fallos de seguridad. 
- Análisis de aplicaciones web: escaneo de vulnerabilidades en la web.
- Evaluación de bases de datos: encuentra vulnerabilidades en una base de datos.
- Ataques de contraseñas: ataques de diccionario, tablas y fuerza bruta.
- Ingeniería inversa: herramientas que consiguen el código fuente de una aplicación.
- Herramientas de explotación: sirve para aprovechar fallos de seguridad para infiltrarse en el sistema.
- Sniffing and Spoofing: robo de datos y suplantación de identidad.
- Postexplotación: ejecuta tareas dañinas.
- Análisis forense: se estudian las huellas que deja el ciberdelincuente.
- Herramientas de reporte: sirven para diseñar informes de ciberseguridad.
- Herramientas de seguridad social: se utiliza para simular campañas de phishing (correos falsos), spear phishing (phishing para una persona específica o un grupo específico).
## Requisitos
- UI: Sin interfaz gráfica de usuario, se necesitan al menos 128 MB de memoria RAM y 2 GB de almacenamiento en disco. Sin embargo, se recomienda tener al menos 512 MB de memoria RAM.
- GUI: En el entorno de escritorio, se necesitan 8 GB de memoria RAM y 20 GB de espacio de almacenamiento para garantizar que el sistema funcione de manera permanente y sin degradación.