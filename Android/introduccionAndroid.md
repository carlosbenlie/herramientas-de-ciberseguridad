# Android
## En qué consiste
El último ejemplo de ataque que se va a realizar va a ser sobre una máquina virtual Android, la cual se encuentra en la misma red que la máquina atacante (Kali Linux). Con este ataque se pretende demostrar lo fácil que se pueden vulnerar estos sistemas si no se siguen medidas de seguridad básicas como no instalar aplicaciones que no se encuentren en la tienda oficial de tu dispositivo, ya que al instalarlo de fuentes ajenas a las oficiales pueden tener cualquier tipo de malware y se puede correr un gran peligro. 

El ataque que se va a realizar consiste en crear una apk para Android la cual va a servir para obtener todos los datos del dispositivo y estos serán mandados a otra máquina, la del atacante. 

Se van a utilizar varias herramientas, la primera es conocida como __msfvenom__, la cual sirve para crear esta apk que hemos comentado antes. Este programa funciona mediante payload, que es la parte del código del malware que realiza las acciones maliciosas en el sistema (robo de información, suplantación de identidad, borrado de ficheros…). Los payload se ejecutan mediante exploit, estos exploit son la parte encargada de aprovechar la vulnerabilidad, y permiten ejecutar el payload.

La segunda herramienta que se va a utilizar es __msfconsole__, que lo que hará es desplegar un centro de control, donde se podrá interactuar con la máquina infectada. Se deberá haber instalado previamente en la máquina víctima la aplicación infectada.

Para descargar la aplicación en la máquina de la víctima, es decir en Android, al tratarse de máquinas virtuales se ha optado por hacerlo con un servidor web básico utilizando Python 2, aunque se podría haber hecho de otras formas, pero ha sido la más rápida y efectiva. 
